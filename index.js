
    apiKey = 'a463eca1785e6c2582d9c2192acb82e8';
    apiToken = '82b431148b1e8c2bd405cebf432346ed37f8588140369af282f76834730b1796';

    /*display saved cards*/
    function onLoadPage(){
        var apiEndpoint1 = 'https://api.trello.com/1/lists/5e6f59be99a2d684da8be6ef/cards?key='+apiKey+'&token=' +apiToken;
        var request = new XMLHttpRequest();
        request.open('GET', apiEndpoint1 , 'true');
        request.send();
        request.onload = () => {

            if (request.status===200){
                var data = JSON.parse(request.response);
                count = data.length;
                document.getElementsByClassName('count')[0].innerText=count;
                if (data.length != 0){
                    data.forEach(card=>{
                        var listElement = document.createElement("LI");
                        listElement.className = 'list-of-cards'
                        listElement.id = card.id;
                        var cardName = document.createTextNode(card.name);
                        listElement.append(cardName);
                        listElement.addEventListener("click", modify(listElement));
                        var childOf = document.getElementById('card-list');
                        childOf.appendChild(listElement);
                        addCloseBtn(listElement);
                        })
                }

            }
        
        
            else{
                alert(`error ${request.status} ${request.statusText}`);
                }
        
            }
        }

    /*display new added cards in UI*/ 
    function displayCard(event, id){
        
                var listElement = document.createElement("LI");
                listElement.className = 'list-of-cards';
                listElement.id = id;
                var cardName = document.createTextNode(event);
                listElement.append(cardName);
                var childOf = document.getElementById('card-list');
                listElement.addEventListener("click", modify(listElement));
                childOf.appendChild(listElement);
                addCloseBtn(listElement);

        }

    /*add card in trello*/
    function addCard(event){
        if (event.keyCode===13){
             if (event.currentTarget.value!=""){
            count = count+1;
            document.getElementsByClassName('count')[0].innerText=count;
            cardName = String(event.currentTarget.value);
            apiEndpoint = "https://api.trello.com/1/cards?name=" +cardName+ "&idList=5e6f59be99a2d684da8be6ef&key="+apiKey+"&token="+apiToken;
            var req = new XMLHttpRequest();
            req.open('POST', apiEndpoint , 'true');
            req.send();
            req.onload = ()=>{
                if (req.status===200){
                    cardResponse= JSON.parse(req.response);
                    displayCard(cardName,cardResponse["id"]);
                    }
                else{
                    alert(`error ${request.status} ${request.statusText}`);
                }
        }
            event.currentTarget.value=''; 
            
             }
            else{
                alert("Card name cannot be empty");
            }
        }
     }

        
    
    /*delete card in trello and UI*/ 
    function deleteCard(close){
            close.onclick = function() {
                    var confirmation = confirm("Delete the card?");
                    if (confirmation==true){
                    count = count-1;
                    document.getElementsByClassName('count')[0].innerText=count; 
                    var div = this.parentNode;
                    apiEndpoint = "https://api.trello.com/1/cards/" +close["id"]+ "?&key="+apiKey+"&token="+apiToken;
                    var req = new XMLHttpRequest();
                    req.open('DELETE', apiEndpoint);
                    req.send();            
                    div.remove(close);//remove from UI
                    }
            }
            


     }

    /*add close button to each list item in UI */
    function addCloseBtn(cardList){
                    var span = document.createElement("SPAN");
                    var txt = document.createTextNode("\u00D7");
                    span.className = "close";
                    span.id=cardList.id;
                    span.appendChild(txt);
                    cardList.appendChild(span);
                    span.addEventListener("click", deleteCard(span));
                }

    /*modify card name in trello and UI */
    function modify(item){
                item.ondblclick=function(){
                var modify = prompt("Enter a new name");
                if (modify!="" && modify!=null){
                    item.textContent = modify;//modified name assigned in UI
                    var req = new XMLHttpRequest();
                    apiEndpoint = "https://api.trello.com/1/cards/" +item.id+ "?&name=" +item.textContent+ "&key="+apiKey+"&token="+apiToken;
                    req.open('PUT', apiEndpoint , 'true');
                    req.send();
                    addCloseBtn(item);
                }
        }

    }
