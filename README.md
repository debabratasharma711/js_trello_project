This project uses trello API and uses concepts of DOM. This project is done using html, css and javascript.

## Tasks:
    Create a UI that interacts with a trello list of the board.
    Interactions include:
        Display all the cards of the list.
        Add a card from UI.
        Delete a card from UI.
        Modify a card from UI.
## File description
    index.html file contains the basic layout of the page.
    index.css file contains the style of the page.
    index.js file contains all the DOM implementation.

TO EXECUTE THE FILE RUN index.html